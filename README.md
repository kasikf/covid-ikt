# Analytické úkoly:

- souvislost mezi propouštěním zaměstnanců v tech společnostech s koncem covidu [asi hotovo]
- souvislost mezi počtem přijatých zaměstnanců v tech (bylo potřeba více lidí = více přijatých do technologických společností)
- souvislost mezi počtem nakažených/mrtvých/očkovaných ve světě [HOTOVO]
- souvislost mezi počtem kuřáků a úmrtí [HOTOVO]
- souvislost mezi přísností opatření a počtem nových případů [HOTOVO]
- propad ekonomiky za doby covidu (S&P 500) [HOTOVO]

Další na výběr:

- počet domácností s počítačem (kvůli home office nárůst)
- psychologické efekty covidu

## Backlog:

### 27. 2.

#### Kasík

- založení projektu, nástřel úkolů

### 30. 3.

#### Kasík

- vytvoření dashboardu pro souvislost mezi úmrtími, nakažanými a očkovanými

### 31. 3.

#### Kasík

- vytvoření dashboardu pro souvislost mezi počtem kuřáků a počtem úmrtí
